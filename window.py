from tkinter import *
from tkinter import messagebox
from process import reset_fact_db, main_process


class Window(Frame):

    def __init__(self, master=None):

        Frame.__init__(self, master)

        self.master = master

        self.init_window()

    def makeform(self, event = None):
        fields = 'Nombre côtés', 'Nombre angle droits', 'Nombre de côtés de même taille', 'Nombre de côtés parallèles'
        entries = []
        for field in fields:
            row = Frame(self)
            lab = Label(row, width=50, text=field, anchor='w')
            ent = Entry(row)
            row.pack(side=TOP, fill=X, padx=5, pady=5)
            lab.pack(side=LEFT)
            ent.pack(side=RIGHT, expand=YES, fill=X)
            entries.append((field, ent))
        return entries

    # Creation of init_window
    def init_window(self):

        self.master.title("App")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        entries = self.makeform()

        test_button = Button(self, text="Run", command=lambda: self.client_dotest(entries))
        quit_button = Button(self, text="Exit", command=self.client_exit)
        reset_button = Button(self, text="Reset", command=lambda: self.reset())

        reset_button.pack(side=BOTTOM)
        quit_button.pack(side=BOTTOM)
        test_button.pack(side=BOTTOM)

    def client_exit(self):
        exit()

    def reset(self):
        reset_fact_db()
        messagebox.showinfo("Reset fact base", "La base de faits a été réinitialisée")


    def client_dotest(self, inputs):
        testbool = False
        data = ""
        delimiter = ";"
        for input in inputs:
            field = input[0]
            text = input[1].get()
            data += text + delimiter
            if not text:
                testbool = True
        data += "none"
        if testbool == True:
            messagebox.showinfo("Not good", "At least one field is empty")
        else:
            figure_name = main_process(data)
            if not figure_name:
                messagebox.showinfo("Test", "This figure doesn't exist")
            else:
                messagebox.showinfo("Test", figure_name)


root = Tk()

root.geometry("500x200")

# creation of an instance
app = Window(root)

# mainloop
root.mainloop()