import csv


csv_file = 'fact_base.csv'
csv_delimiter = ';'


def main_process(data):
    # formed by 4 input
    # nrb side, nbr right angle, same length side, nbr parallel side
    # Test fact db
    try:
        figure = None
        with open(csv_file, 'rt') as f:
            reader = csv.reader(f, delimiter=csv_delimiter)
            figure = check_fact_db(data, reader)
    except IOError:
            f = open(csv_file, "a+")
            f.close()
    # Test rule if figure is None
    if figure is None:
        data_send = data.split(csv_delimiter)[:-1]
        figure = check_rules(data_send)
    return figure


figure_name = {
    3: 'triangle',
    4: 'quadrilatere',
    5: 'pentagone'
}


def add_fact_base_line(data, figure):
    with open(csv_file, "a") as f:
        data_write = csv_delimiter.join(data)
        data_write += csv_delimiter + figure
        f.write(data_write)
        f.write('\n')


def check_rules(data):
    figure = define_name(int(data[0]))
    if figure is None:
        return 'We cannot find this figure'
    if figure != 'pentagone':
        figure = define_angle(figure, int(data[1]))
        figure = define_length(figure, int(data[2]))
        figure = define_parallel(figure, int(data[3]))
    if figure:
        add_fact_base_line(data, figure)
    return figure


def define_name(nbr_side):
    return figure_name.get(nbr_side, None)


def define_angle(figure, nbr_angle):
    if figure == 'triangle':
        if nbr_angle == 0:
            return 'triangle'
        elif nbr_angle == 1:
            return 'triangle_rectangle'
        else:
            return None
    elif figure == 'quadrilatere':
        if nbr_angle == 0 or nbr_angle == 1 or nbr_angle == 2:
            return 'quadrilatere'
        elif nbr_angle == 3 or nbr_angle == 4:
            return 'rectangle_ou_carre'
        else:
            return None
    else:
        return None


def define_length(figure, nbr_longueur):
    if figure:
        if figure == 'triangle':
            if nbr_longueur == 0:
                return 'triangle'
            if nbr_longueur == 2:
                return 'triangle_isocele'
            elif nbr_longueur == 3:
                return 'triangle_equilateral'
            else:
                return None
        elif figure == 'triangle_rectangle':
            if nbr_longueur == 0:
                return 'triangle_rectangle'
            elif nbr_longueur == 2:
                return 'triangle_rectangle_isocele'
            else:
                return None
        elif figure == 'quadrilatere':
            if nbr_longueur == 4:
                return 'losange'
            elif nbr_longueur == 2:
                return 'quadrilatere_ou_parallelogramme'
            else:
                return 'quadrilatere'
        elif figure == 'rectangle_ou_carre':
            if nbr_longueur == 2:
                return 'rectangle'
            elif nbr_longueur == 4:
                return 'carre'
        else:
            return None
    return None


def define_parallel(figure, nbr_parallel):
    if figure:
        if 'triangle' in figure:
            if nbr_parallel == 0:
                return figure
            else:
                return None
        elif 'losange' in figure:
            if nbr_parallel == 0:
                return figure
            else:
                return None
        elif figure == 'quadrilatere':
            if nbr_parallel == 0:
                return 'quadrilatere_quelconque'
            elif nbr_parallel == 2:
                return 'trapeze'
            elif nbr_parallel == 4:
                return 'rectangle'
            else:
                return None
        elif figure == 'quadrilatere_ou_parallelogramme':
            if nbr_parallel == 4:
                return 'parallelogramme'
            else:
                return 'quadrilatere'
        elif figure == 'rectangle' or figure == 'carre':
            if nbr_parallel == 4:
                if figure == 'rectangle':
                    return 'rectangle'
                elif figure == 'carre':
                    return 'carre'
            else:
                return None
    return None


def check_fact_db(data, reader):
    find = False
    for line in reader:
        count = 0
        for d in data.split(csv_delimiter)[:-1]:
            if line:
                if d != line[count]:
                    find = False
                    break
                else:
                    count += 1
                    if count == len(data.split(csv_delimiter)[:-1]):
                        find = True
                        break
        if find:
            break
    if find:
        return line[-1]
    return None


def reset_fact_db():
    with open(csv_file, "w") as f:
        f.truncate()


__all__ = [
    'main_process',
    'reset_fact_db'
]
